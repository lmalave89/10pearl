$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Login.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: malave.luis.89@gmail.com"
    }
  ],
  "line": 2,
  "name": "Login GITLAB",
  "description": "",
  "id": "login-gitlab",
  "keyword": "Feature"
});
formatter.before({
  "duration": 22637483400,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Login gitlab success",
  "description": "",
  "id": "login-gitlab;login-gitlab-success",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@10Pearl"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "open url gitab",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "enter credentials",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "view homepage gitlab",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.login_in_Gmail()"
});
formatter.result({
  "duration": 2453294601,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.enter_credentials()"
});
formatter.result({
  "duration": 1964056900,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.view_homepage_gitlab()"
});
formatter.result({
  "duration": 19139001,
  "status": "passed"
});
formatter.after({
  "duration": 2580624300,
  "status": "passed"
});
});