/**
 * 
 */
package utility;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import injectionDependenciesManager.TestContext;

/**
 * @author Lmalave89
 *
 */
public class Hook {
	
	TestContext testContext;

	public Hook(TestContext context) {
		testContext = context;
	}

	
	@Before
	public void beforeScenario(Scenario scenario) {
		
	}	
	
	
	@After
	public void tearDown() throws InterruptedException
	{
		testContext.getGenericsFunctionsPage().closeSessionDriver();
	}
	
}
