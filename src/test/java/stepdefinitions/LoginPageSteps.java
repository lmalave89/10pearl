package stepdefinitions;

import dataFeatures.DataUserGitlab;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import injectionDependenciesManager.TestContext;
import pageObjects.LoginPage;


public class LoginPageSteps{

	TestContext testContext;
	LoginPage loginPage;	
	DataUserGitlab dataUserGitlab = new DataUserGitlab();
	
	public LoginPageSteps(TestContext context) {
		testContext = context;
		loginPage = testContext.getPageObjectManager().getLoginPage();	
	}

	@Given("^open url gitab$")
	public void login_in_Gmail(){
		loginPage.navigateTo_LoginPage();
	}
	
	@When("^enter credentials$")
	public void enter_credentials(){
		testContext.getGenericsFunctionsPage().sendDataToElement(loginPage.username, dataUserGitlab.getUsername());
		testContext.getGenericsFunctionsPage().sendDataToElement(loginPage.password, dataUserGitlab.getPassword());
		testContext.getGenericsFunctionsPage().clickElement(loginPage.buttonLogin);
		testContext.getGenericsFunctionsPage().checkElementVisibleByElement(loginPage.logo);
	}
	
	@Then("^view homepage gitlab$")
	public void view_homepage_gitlab(){
		testContext.getGenericsFunctionsPage().checkElementVisibleByElement(loginPage.logo);
	}
}
