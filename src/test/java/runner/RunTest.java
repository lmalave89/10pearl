package runner;

import org.testng.annotations.Test;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


/**
 * @author Lmalave89
 *
 */

@CucumberOptions(features={"src//test//java//features"}
					,glue={"stepdefinitions","utility"}
					,plugin = {"pretty", "html:target/cucumber"}
					, tags ={"@10Pearl"}
		)
@Test
public class RunTest extends AbstractTestNGCucumberTests{

}
