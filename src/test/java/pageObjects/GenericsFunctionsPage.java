package pageObjects;

import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import managers.FileReaderManager;

public class GenericsFunctionsPage {

	WebDriver driver;

	public GenericsFunctionsPage(WebDriver driver) {
		this.driver = driver;
	}

	public void checkElementVisibleByElement(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(this.driver,FileReaderManager.getInstance().getConfigReader().getImplicitlyWait());			 
	        wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			System.out.println("Not found this element " + element + " for this time " + String.valueOf(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait()));
		}
	}
	
	public void checkElementVisibleByElementList(List<WebElement> element) {
		try {
			WebDriverWait wait = new WebDriverWait(this.driver,FileReaderManager.getInstance().getConfigReader().getImplicitlyWait());			 
	        wait.until(ExpectedConditions.visibilityOfAllElements(element));
		} catch (Exception e) {
			System.out.println("Not found this element " + element);
		}
	}	

	// Click functions by Identifier

	public void clickElement(WebElement element){
		this.checkElementVisibleByElement(element);
		element.click();
	}

	// send value functions by Identifier
	public void sendDataToElement(WebElement element, String value){
		this.checkElementVisibleByElement(element);
		element.sendKeys(value);
	}

	// Click functions select value in list by position 
	public void clickInListByPosition(List<WebElement> element, int position){
		this.checkElementVisibleByElementList(element);
		element.get(position).click();
	}
	
	// Click functions select value in list by text
	public void clickInListByText(List<WebElement> elements, String value){
		this.checkElementVisibleByElementList(elements);
		for (WebElement element : elements) {
			if (element.getText().equals(value)) {
				element.click();
				break;
			}
		}

	}
	
	// Click functions select value in list by text autocompletare
	public void clickInListByText(WebElement element, String value){
		this.checkElementVisibleByElement(element);
		element.sendKeys(value);
		element.sendKeys(Keys.DOWN);
		element.sendKeys(Keys.ENTER);


	}
	
	// Check text by Identifier
	public boolean verificTextElement(WebElement element, String value){
		this.checkElementVisibleByElement(element);
		if (element.getText().equals(value)) {
			return true;
		}
		return false;
	}

	public void scrollPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
	}

	public void scrollPageVerticalAll() {
		org.openqa.selenium.Dimension initial_size = driver.manage().window().getSize();
		int height = initial_size.getHeight();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0," + Integer.toString(height) + ")");
	}

	public void scrollPageHalfVerticalAll() {
		org.openqa.selenium.Dimension initial_size = driver.manage().window().getSize();
		int height = initial_size.getHeight();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0," + Integer.toString(height / 2) + ")");
	}

	public void scrollPageHorizontalAll() {
		org.openqa.selenium.Dimension initial_size = driver.manage().window().getSize();
		int width = initial_size.getWidth();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(" + Integer.toString(width) + ",0)");
	}

	public void clickInListByValue(List<WebElement> elements, String value){
		this.checkElementVisibleByElementList(elements);
		for (WebElement element : elements) {
			if (element.getText().equals(value)) {
				element.click();
				break;
			}
		}
		System.out.println("Not found this value in list " + value);
		System.exit(0);
	}

	public boolean verificInListByValue(List<WebElement> elements, String value){
		this.checkElementVisibleByElementList(elements);
		for (WebElement element : elements) {
			if (element.getText().equals(value)) {
				return true;
			}
		}
		return false;
	}

	public void closeSessionDriver(){
		driver.quit();
	}

}
