package pageObjects;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import DataFeatures.DataUserGmail;
import managers.FileReaderManager;

public class LoginPage {
	
	WebDriver driver;
	FileReaderManager fileReaderManager; 
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.ID, using = "user_login") 
	public  WebElement username;
	
	@FindBy(how = How.ID, using = "user_password") 
	public WebElement password;
	
	@FindBy(how = How.NAME, using = "commit") 
	public WebElement buttonLogin;		
	
	@FindBy(how = How.ID, using = "logo") 
	public  WebElement logo;
		
	public void navigateTo_LoginPage() {
		driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
	}		
}
	