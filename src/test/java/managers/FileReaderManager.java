package managers;

import dataProvider.ConfigFileReader;

public class FileReaderManager {

	private static FileReaderManager FileReaderManager = new FileReaderManager();
	private static ConfigFileReader ConfigFileReader;
	
	public FileReaderManager() {
	}

	 public static FileReaderManager getInstance() {
	      return FileReaderManager;
	 }

	 public ConfigFileReader getConfigReader() {
		 return (ConfigFileReader == null) ? new ConfigFileReader() : ConfigFileReader;
	 }
}
