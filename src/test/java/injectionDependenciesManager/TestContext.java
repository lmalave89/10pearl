package injectionDependenciesManager;

import managers.PageObjectManager;
import managers.WebDriverManager;
import pageObjects.GenericsFunctionsPage;

public class TestContext {

	private WebDriverManager webDriverManager;
	private PageObjectManager pageObjectManager;
	private GenericsFunctionsPage genericFunctionsPage;
	
	public TestContext() {
		webDriverManager = new WebDriverManager();
		pageObjectManager = new PageObjectManager(webDriverManager.getDriver());
		genericFunctionsPage = new GenericsFunctionsPage(getWebDriverManager().getDriver());	
	}

	public WebDriverManager getWebDriverManager() {
		return webDriverManager;
	}
	
	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}
	
	public GenericsFunctionsPage getGenericsFunctionsPage() {
		return genericFunctionsPage;
	}
}
